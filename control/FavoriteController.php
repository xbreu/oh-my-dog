<?php

require_once "../model/Favorites.php";

class FavoriteController extends Controller {
    public function favorite() {
        Favorites::togglePet($_GET["id"]);
        $from = isset($_GET["from"]) ? $_GET["from"] : "pets";
        header("location: " . $from);
    }
}