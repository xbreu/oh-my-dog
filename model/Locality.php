<?php


class Locality extends ActiveRecord
{
    private $id;
    private $table;
    
    /**
     * Locality constructor.
     *
     * @param $table
     */
    public function __construct($table) { $this->table = $table; }
    
    public function tableName()
    {
        return $this->table;
    }
    
    public function rules()
    {
        return [];
    }
    
}