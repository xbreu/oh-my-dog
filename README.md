# OH MY DOG!
Welcome to the documentation of  g30-2020/2021 LWT project!

* [Elements](#Elements)
* [How to access](#How-to-access)
* [Features](#Features)
* [Libraries used](#Libraries)
* [Requirements](#Requirements) 
* [Architecture and Design](#Architecture-and-Design)
* [Appearance](#Appearance)

---
## Elements
* Alexandre Abreu              (201800168)
* Amanda de Oliveira Silva     (201800698)
* Guilherme Candicella Calassi (201800157)

---
## How to access
Our web page can not be acess by gnomo because of the way we handle the URLs. Although following the next steps you will be able to acess it:
```
git clone https://github.com/FEUP-LTW/ltw-project-g30
cd ltw-project-g30/public
php -S localhost:4000
```
Alternatively you can use this [link](https://www.ohmydog.yourmove.com.br/).

All users have the same funtionality, but if you want some already created users:
* alexandre/12345
* amanda/12345
* guilherme/12345

---
## Features
* Security
    * XSS: yes
    * CSRF: yes
    * SQL using prepare/execute: yes
    * Passwords: bcrypt (php's native `password_hash` function with the bcrypt algorithm)
    * Data Validation: php (we used its native functions for things that would use a regex)
* Technologies
    * Separated logic/database/presentation: yes
    * Semantic HTML tags: yes
    * Responsive CSS: yes
    * Javascript: yes
    * Ajax: yes (to filter counties after a district is chosen)
    * REST API: partial (We have an API to retrieve the localities, counties and districs with some parameters using the GET method)
* Usability:
    * Error/success messages: yes
    * Forms don't lose data on error: yes (only the passwords are lost)

---
## Libraries
* Icon fonts were used with the purpose of improving the design an aperence. 
    * [Material Icons](https://material.io/resources/icons/?style=baseline)
    * [Font Awesome](https://fontawesome.com/)

---
## Architecture and Design
* The MVC model was the organizational pattern chosen for the high-level structure division of the project, so we have:
    * __Models__ store data and know how to treat, validade, and read/save them on the database.
    * __Views__ are responsible for the interface and user experience, they also use predefined layouts for each group of pages.
    * __Controllers__ handle the treatment of the received data and call the Models validation functions and Views render.
* Friendly URLs for a smooth navigation, link aesthetics and request treatment by the Controllers.

---
## Requirements 

#### All users are able to:
* Register a new account.
* Login and logout.
* Edit their profile.
#### Users that found a pet and are looking for someone to adopt:
* Add information about the pet. Including name,species (e.g., dog, cat), size, color, photo and location.
* Manage previous postings.
* List any questions, inquiries, and adoption proposals.
* Accept or refuse adoption proposals.
#### All users should be able to (users can simultaneously be looking for a pet, or have a pet for a adoption):
* Register a new account.
* Login and logout.
* Edit their profile (username and password at least).
#### Users that found a pet and are looking for someone to adopt it should be able to:
* Add information about the pet. Including name (if any), species (e.g., dog, cat), size, color, location, …
* Manage previous postings.
* List any questions, inquiries, and adoption proposals.
* Accept or refuse adoption proposals.
#### Users looking for a pet should be able to:
* Search for a pet using several search criteria.
* Add pets to a favourites list.
* Ask questions about a pet listed for adoption.
* Propose to adopt a pet and wait until the owner accepts or refuses their proposal

---
## Appearance
Appearance of some main pages:

#### Home page
![](docs/home.gif)

#### Login
![](docs/login.gif)

#### Dashboard
![](docs/dashboard.gif)

#### Wishlist
![](docs/favorites.gif)

#### Publication and comments
![](docs/publication.gif)
