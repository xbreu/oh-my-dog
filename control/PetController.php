<?php
require_once "../core/Controller.php";
require_once "../model/Comment.php";

class PetController extends Controller {
    public function __construct() {
        parent::setLayout('list');
    }

    public function feed(Request $request) {
        $pubs = new Publication();
        $pubs = $pubs->findAll();
        $params = [
            'pets' => $pubs,
            'favorites' => Favorites::get()
        ];

        return $this->render('pets/feed', $params);
    }

    public function item(Request $request) {
        parent::setLayout("publication");
        if ($request->isPost()) {
            $newComment = new Comment();
            $newComment->idPublication = $_POST['publication'];
            $newComment->comment = $_POST['comment'];
            $newComment->idUser = Application::user()['id'];
            $newComment->proposal = isset($_POST['proposal']);
            $newComment->insert();
            $id = $_POST['publication'];
        } else {
            $id = $_GET['id'];
        }

        $publication = new Publication();
        $publication = $publication->findWhere(['id' => $id])[0];
        $pet = new Pet();
        $pet = $pet->findWhere(['id' => $publication->idPet])[0];
        $user = new Customer();
        $user = $user->findWhere(['id' => $publication->idUser])[0];
        $county = new Locality("County");
        $county = $county->findWhere(['id' => $user->countyCode])[0];
        $comments = new Comment();
        $comments = $comments->findWhere(['idPublication' => $publication->id]);
        if (empty($publication) || empty($pet) || empty($user)) throw new ErrorNotFound();
        return $this->render('pets/publication', [
            'publication' => $publication,
            'pet' => $pet,
            'user' => $user,
            'county' => $county,
            'comments' => $comments
        ]);

    }
}