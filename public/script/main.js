/*
*
* OUR MAIN JS
*
*/

function toggleOptions(name) {
    document.getElementById('select-search-' + name).classList.toggle("show");
    for (let el of document.querySelectorAll('[id^=select-search-]')) {
        if (el.id !== 'select-search-' + name)
            el.classList.remove("show");
    }
}

function filterOptions(name) {
    let input, filter, a, i;
    input = document.getElementById('input-search-' + name);
    filter = input.value.toUpperCase();
    div = document.getElementById('select-search-' + name);
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
        txtValue = a[i].textContent || a[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            a[i].style.display = "";
        } else {
            a[i].style.display = "none";
        }
    }
}

function setSelectSearch(option, value, name, callback = null) {
    var input = document.getElementById('input-search-' + name);
    var x = document.getElementById(name);
    input.value = option;
    x.value = value;
    if (callback !== null) callback(value)
}

function addButtonForm(name) {
    let button = document.getElementById(name + "-button")
    if (button === null)
        return;
    button.onclick = function () {
        document.getElementById(name + "-form").submit()
    }
}

addButtonForm("favorite")
addButtonForm("logout")

//////////////////////////////////////////////////////////
//         MODEL PUBLICATION                            //
//////////////////////////////////////////////////////////

// Get the modal
let modal = document.getElementById("proposal-modal");
let btn = document.getElementById("modal-button");
let span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
if (btn !== null) btn.onclick = function () {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
if (span !== undefined) span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
}
