<?php
require_once "../core/Application.php";

class Controller {
    public $layout = 'main';

    public function setLayout($layout) {
        $this->layout = $layout;
    }

    public function render($view, $params = []) {
        return Application::$app->router->renderView($view, $params);
    }
}
