<?php
if (!Application::isGuest()) {
    echo '<section class="new-pub">';
    echo '<a href="new-publication">New Publication</a>';
    echo '</section>';
}
?>

<aside>
    <header>Filters</header>
    <input type="radio" name="gender" value="m"> M
    <input type="radio" name="gender" value="f"> F
    <input type="radio" name="gender" value=""> Clear
    <br>
    Max Age: <input type="number" name="age" min="0">
</aside>

<section id="feed">
    <?php foreach ($pets as $pub) {
        $pet = new Pet();
        $pet = $pet->findWhere(['id' => $pub->idPet])[0];
        ?>

        <article class="pet">
            <div class="image">
                <?php
                $photo = new Photo();
                echo '<img src="data:image/jpg;base64,' . base64_encode($photo->findWhere(['id' => $pet->idPhoto])[0]->img) . '" alt="' . $pet->name . ' photo" />';
                ?>
                <div class="actions">
                    <a href="favorite?id=<?= $pub->id ?>"
                       class="favorite <?= ($favorites !== null && in_array($pub->id, $favorites)) ? "already" : "" ?>">
                        <i class="material-icons">favorite<?= ($favorites !== null && in_array($pub->id, $favorites)) ? "_border" : "" ?></i>
                    </a>
                    <a href="pet?id=<?= $pub->id ?>" class="go"><i class="material-icons">link</i></a>
                </div>
            </div>
            <footer>
                <i class="fas <?= ($pet->gender === "m") ? "fa-mars" : "fa-venus" ?> gender"></i>
                <span class="name"><?= $pet->name ?></span> -
                <span class="age"><?= !empty($pet->birth) ? date('Y') - explode("-", $pet->birth)[0] : "" ?></span>
            </footer>
        </article>
    <?php } ?>
</section>

<script>
    let filters = []

    function filter() {
        let pets = document.getElementsByClassName("pet")
        for (let pet of pets) {
            pet.style.display = "block"
        }
        for (let key in filters) {
            if (!filters.hasOwnProperty(key) || filters[key] == false)
                continue
            let value = filters[key]
            for (let pet of pets) {
                switch (key) {
                    case "gender":
                        let petGender = pet.getElementsByClassName("gender")[0]
                        if (petGender.classList.contains("fa-mars") && value !== "m")
                            pet.style.display = "none"
                        else if (petGender.classList.contains("fa-venus") && value !== "f")
                            pet.style.display = "none"
                        break
                    case "age":
                        let petAge = parseInt(pet.getElementsByClassName("age")[0].innerHTML)
                        if (petAge > value)
                            pet.style.display = "none"
                }
            }
        }
    }

    let genders = document.getElementsByName("gender")
    genders.forEach(function (gender) {
        gender.addEventListener("change", function () {
            filters['gender'] = gender.value
            filter()
        }, false)
    })

    let age = document.getElementsByName("age")[0]
    age.addEventListener("change", function () {
        filters['age'] = age.value
        filter()
    }, false)

</script>