<?php

class Favorites {
    static $EXPIRATION_TIME = 60; // In minutes
    static $COOKIE_NAME = "favorites";
    static $COOKIE_DELIMITER = ",";

    static function createCookie($val = "") {
        $time = time() + self::$EXPIRATION_TIME * 60;
        setcookie(self::$COOKIE_NAME, $val, $time);
    }

    static function removeCookie() {
        setcookie(self::$COOKIE_NAME, "", time() - 3600);
    }

    static function togglePet($petId) {
        if (!isset($_COOKIE[self::$COOKIE_NAME]))
            self::createCookie();

        $list = self::get();
        if ($list !== null && ($key = array_search($petId, $list)) !== false)
            unset($list[$key]);
        else
            $list[] = $petId;

        self::removeCookie();
        self::createCookie(implode(self::$COOKIE_DELIMITER, $list));
    }

    static function get() {
        if (!isset($_COOKIE[self::$COOKIE_NAME]))
            return null;
        return explode(self::$COOKIE_DELIMITER, $_COOKIE[self::$COOKIE_NAME]);
    }
}