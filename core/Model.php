<?php

abstract class Model
{
    const RULE_REQUIRED = 'required';
    const RULE_EMAIL = 'email';
    const RULE_SIZE = 'size';
    const RULE_MATCH = 'match';
    const RULE_AUTH = 'auth';
    
    public function loadData($data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }
    
    abstract public function rules();
    
    public $errors = [];
    
    public function addError($attribute, $message)
    {
        $this->errors[$attribute][] = $message;
    }
    
    public function validate()
    {
        foreach ($this->rules() as $attribute => $rules) {
            $value = $this->{$attribute};
            foreach ($rules as $rule) {
                $ruleName = $rule;
                if (!is_string($ruleName)) {
                    $ruleName = $rule[0];
                }
                switch ($ruleName) {
                    case self::RULE_REQUIRED:
                        if (empty($value)) {
                            $this->addError($attribute, "This field is required");
                        }
                        break;
                    case self::RULE_SIZE:
                        {
                            $minError = isset($rule['min']) && strlen($value) < $rule['min'];
                            $maxError = isset($rule['max']) && strlen($value) > $rule['max'];
                            if (($minError && isset($rule['max'])) || ($maxError && isset($rule['min']))) {
                                $this->addError($attribute,
                                    "This field must have between " . $rule['min'] . " and " . $rule['max'] . " characters");
                            } elseif ($minError) {
                                $this->addError($attribute,
                                    "This field must have at least " . $rule['min'] . " characters");
                            } elseif ($maxError) {
                                $this->addError($attribute,
                                    "This field must have at maximum " . $rule['max'] . " characters");
                            }
                        }
                        break;
                    case self::RULE_MATCH:
                        if ($value !== $this->{$rule['match']}) {
                            $this->addError($attribute, "This field must match " . $rule['match']);
                        }
                        break;
                    case self::RULE_EMAIL:
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            $this->addError($attribute, "This field must be a valid email");
                        }
                        break;
                    case self::RULE_AUTH:
                        {
                            $customer = new Customer();
                            $customer = $customer->findWhere(['username' => $value]);
                            if (!empty($customer)) {
                                if (!password_verify($this->{$rule['auth']}, $customer[0]->password)) {
                                    $this->addError($rule['auth'], "Wrong password");
                                }
                            } else {
                                $this->addError($attribute, "This user does not exist");
                            }
                            
                            return empty($this->errors) ? $customer : false;
                        }
                        break;
                }
            }
        }
        
        return empty($this->errors);
    }
}
