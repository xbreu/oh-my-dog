<?php

require_once "../core/Controller.php";
require_once "../tags/Form.php";
require_once "../model/LoginForm.php";
require_once "../model/SignUpForm.php";
require_once "../model/Customer.php";
require_once "../model/Locality.php";
require_once "../model/PublicationForm.php";

class DashboardController extends Controller {
    public function __construct() {
        parent::setLayout('dashboard');
    }

    public function dashboard() {
        if (Application::isGuest()) {
            Application::goHome();
        }
        $customer = new Customer();
        $customer = $customer->findWhere(['id' => Application::user()['id']])[0];
        if (empty($customer)) {
            Application::goHome();
        }

        $menu = [
            '/myaccount' => 'My Account',
            '/mypublications' => 'My Publications',
            '/myproposals' => 'My Proposals',
            '/mymessages' => 'My Messages',
        ];

        return $this->render('customer/dashboard', ['menu' => $menu, 'customer' => $customer]);
    }

    public function myaccount(Request $request) {
        if (Application::isGuest()) {
            Application::goHome();
        }
        $customer = new Customer();
        $customer = $customer->findWhere(['id' => Application::user()['id']])[0];
        if (empty($customer)) {
            Application::goHome();
        }
        parent::setLayout('auth');

        $model = new Customer();
        $districts = new Locality('District');
        $districts = $districts->findAll();
        $cleanDistricts = [];
        foreach ($districts as $district) {
            $cleanDistricts[$district->id] = $district->name;
        }
        if ($request->isPost()) {
            $model->loadData($request->getBody());
            $model->password = $customer->password;
            $model->id = $customer->id;
            if ($model->validate()) {
                $model->update();
                return $this->render('customer/myaccount', ['form' => $model, 'districts' => $cleanDistricts]);
            }
        }
        $model = $customer;
        return $this->render('customer/myaccount', ['form' => $model, 'districts' => $cleanDistricts]);
    }

    public function mypublications() {
        $pubs = new Publication();
        $pubs = $pubs->findWhere(['idUser' => Application::user()['id']]);
        parent::setLayout('list');
        return $this->render('pets/feed', ['pets' => $pubs, 'favorites' => Favorites::get()]);
    }

    public function myfavorites() {
        $pubs = new Publication();
        $favorites = Favorites::get();
        $list = [];
        foreach ($favorites as $favorite) {
            $list[] = $pubs->findWhere(['id' => $favorite])[0];
        }

        parent::setLayout('list');
        return $this->render('pets/feed', [
            'pets' => $list,
            'favorites' => Favorites::get()
        ]);
    }
}