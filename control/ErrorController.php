<?php
require_once "../core/Controller.php";

class ErrorNotFound extends Exception
{
    /**
     * ErrorNotFound constructor.
     */
    public function __construct()
    {
        parent::__construct();
        header('Location: not-found');
    }
}

class ErrorController extends Controller
{
    public function __construct()
    {
        parent::setLayout('error');
    }
    
    public function not_found()
    {
        return $this->render('not-found');
    }
}