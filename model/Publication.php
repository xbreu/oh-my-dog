<?php


class Publication extends ActiveRecord {
    public $id;
    public $description;
    public $idUser;
    public $idPet;
    public $localityCode;

    public function tableName() {
        return "Publication";
    }

    public function rules() {
        return [];
    }
}