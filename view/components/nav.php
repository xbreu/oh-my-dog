<nav>
    <ul>
        <li class="logo">
            <a href="<?= Application::$BASE_DIR ?>">
                <img src="images/logo.png" alt="logo of oh-my-dog">
            </a>
        </li>
        <!--<li><a href="login">How it Works</a></li>-->
        <li><a href="pets">Pets</a></li>
        <li class="separator"></li>
        <li class="favorites"><a href="myfavorites"><i class="material-icons">favorite</i></a></li>
        <li class="notifications"><a href="login"><i class="material-icons">notifications</i></a></li>
        <li class="account">
            <a href="login"><i class="material-icons">account_circle</i></a>
            <ul class="dropdown-list">
                <?php if (Application::isGuest()) { ?>
                    <li><a href="login">Login</a></li>
                    <li><a href="register">Register</a></li>
                <?php } else { ?>
                    <li><a href="dashboard">Dashboard</a></li>
                    <li>
                        <form id="logout-form" action="logout" method="POST"></form>
                        <a id="logout-button">Logout</a>
                    </li>
                <?php } ?>
            </ul>
        </li>
    </ul>
</nav>
