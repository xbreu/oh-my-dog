<?php
require_once "../model/Publication.php";
require_once "../model/Pet.php";
require_once "../model/Photo.php";

class PublicationForm extends Model
{
    public $img;
    public $description;
    public $name;
    public $birth;
    public $gender;
    public $color;
    public $size;
    public $photo;
    public $breedId;
    public $idUser;
    public $idPet;
    public $localityCode;

    public function rules()
    {
        return [
            'name' => [self::RULE_REQUIRED],
            'birth' => [self::RULE_REQUIRED]
        ];
    }

    public function register()
    {

        $pet = new Pet();
        $pet->loadData($this);
        $publication = new Publication();
        $publication->loadData($this);
        $pet->idPhoto = $this->register_photo();
        if ($pet->validate() && $publication->validate()) {
            if (($publication->idPet = $pet->insert()) !== false) {
                if (($id_publication = $publication->insert()) !== false) {
                    return $id_publication;
                }
            }
        }
        $this->errors = array_merge($this->errors, array_merge($pet->errors, $publication->errors));
        return false;
    }

    public function register_photo()
    {
        $photo = new Photo();
        $img = $_FILES["img"];
        if($img != NULL) {
                $photo->img = file_get_contents($_FILES['img']['tmp_name']);
        }
        return $photo->insert();
    }


}