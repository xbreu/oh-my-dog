<form action="" method="post">
    <i class="material-icons account-image">account_circle</i>

    <?php Form::write([
        ['type' => 'text', 'name' => 'username', 'label' => 'Username'],
        ['type' => 'password', 'name' => 'password', 'label' => 'Password'],
    ], $model); ?>

    <button type="submit">Login</button>

    <a href="register">Not signet yet?</a>
    <a href="register">Forgot your password?</a>
</form>


