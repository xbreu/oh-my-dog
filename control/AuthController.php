<?php

require_once "../core/Controller.php";
require_once "../tags/Form.php";
require_once "../model/LoginForm.php";
require_once "../model/SignUpForm.php";
require_once "../model/Locality.php";

class AuthController extends Controller {
    public function __construct() {
        parent::setLayout('auth');
    }

    public function login(Request $request) {
        $model = new LoginForm();
        if ($request->isPost()) {
            $model->loadData($request->getBody());
            if ($customer = $model->validate()) {
                $this->setSessionCustomer($customer[0]);

                return header('Location: dashboard');
            }
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function register(Request $request) {
        $model = new SignUpForm();
        if ($request->isPost()) {
            $model->loadData($request->getBody());
            if ($model->validate() && $model->register()) {
                $this->setSessionCustomer($model);

                return header('Location: dashboard');
            }
        }

        $districts = new Locality('District');
        $districts = $districts->findAll();
        $cleanDistricts = [];
        foreach ($districts as $district) {
            $cleanDistricts[$district->id] = $district->name;
        }

        return $this->render('register', [
            'model' => $model,
            'districts' => $cleanDistricts
        ]);
    }

    public function logout(Request $request) {
        if ($request->isPost()) {
            unset($_SESSION["customer"]);
        }

        return header('Location: /');
    }

    public function setSessionCustomer($customer) {
        if (!isset($customer->id)) {
            $aux = new Customer();
            $customer = $aux->findWhere(['username' => $customer->username])[0];
        }
        $_SESSION["customer"] = [
            'id' => $customer->id,
            'username' => $customer->username
        ];
    }

    public function createPublication(Request $request) {
        $model = new PublicationForm();
        $model->idUser = Application::user()['id'];
        if ($request->isPost()) {
            $model->loadData($request->getBody());
            if ($model->validate() && ($id = $model->register())) {
                return header('Location: pet?id='.$id);
            }
        }

        return $this->render('customer/addPublication', [
            'model' => $model
        ]);
    }

    public function editPublication() {
        return $this->render('customer/editPublication');
    }
}