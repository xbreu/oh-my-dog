<title>Oh My Dog!</title>
<base href="">

<!-- Default tylesheets for all pages -->
<link rel='stylesheet' type='text/css' href='style/style.css'>
<link rel='stylesheet' type='text/css' href='style/carousel.css'>
<link rel="icon" href="images/logo.png">

<!-- Material icons from Google -->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script src="https://kit.fontawesome.com/fb875d9412.js" crossorigin="anonymous"></script>
