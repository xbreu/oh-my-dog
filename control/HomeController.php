<?php
require_once "../core/Controller.php";

class HomeController extends Controller {
    
    /**
     * HomeController constructor.
     */
    public function __construct() {
        parent::setLayout('index');
    }
    
    public function home(Request $request) {
        $params = [
            'slides' => array_filter(scandir("images/carousel"), function ($value) {
                return !in_array($value, array(".", ".."));
            }),
            'categories' => array_filter(scandir("images/categories"), function ($value) {
                return !in_array($value, array(".", ".."));
            })
        ];
        return $this->render('home', $params);
    }
}