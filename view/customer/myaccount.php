<form action="" method="post">
    <i class="material-icons account-image">account_circle</i>
    
    <?php Form::write([
        ['type' => 'text', 'name' => 'username', 'label' => 'Username'],
        ['type' => 'text', 'name' => 'firstName', 'label' => 'First Name'],
        ['type' => 'text', 'name' => 'lastName', 'label' => 'Last Name'],
        ['type' => 'email', 'name' => 'email', 'label' => 'Email'],
        ['type' => "date", 'name' => 'birth', 'label' => 'Birthday'],
        ['type' => "text", 'name' => 'phone', 'label' => 'Phone Number'],
        [
            'type' => "select-search",
            'name' => 'districtCode',
            'label' => 'District',
            'options' => $districts,
            'callback' => 'appendCounty'
        ],
        ['type' => "select-search", 'name' => 'countyCode', 'label' => 'County'],
    ], $form); ?>
    
    <button type="submit">Save Profile</button>
</form>

<script>

    var district = document.getElementById('input-search-districtCode');
    var district_hidden = document.getElementById('districtCode');

    var county = document.getElementById('input-search-countyCode');
    var county_hidden = document.getElementById('countyCode');
    setDistrictByCountyId();
    getCountyById()


    function appendCounty(event) {
        county.disabled = true;
        var formData = new FormData();
        formData.append("districtId", district_hidden.value);
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                var r = JSON.parse(xmlHttp.responseText);
                var html = "";
                for (var county in r) {
                    html += '<a onclick="setSelectSearch(\'' + r[county] + '\', \'' + county + '\', \'countyCode\')">' + r[county] + '</a>';
                }
                document.getElementById('select-search-countyCode').innerHTML = html;
            }
        }
        xmlHttp.open("post", "/getCountiesByDistrictId");
        xmlHttp.send(formData);
        county.disabled = false;
    }

    function setDistrictByCountyId() {
        var formData = new FormData();
        formData.append("countyId", county_hidden.value);
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                var r = JSON.parse(xmlHttp.responseText);
                var html = "";
                var html2 = "";
                for (var dist in r) {
                    html = r[dist];
                    html2 = dist;
                }

                document.getElementById('input-search-districtCode').value = html;
                document.getElementById('districtCode').value = html2;
            }
        }
        xmlHttp.open("post", "/getDistrictByCountyId");
        xmlHttp.send(formData);
    }
    
    function getCountyById(){
        var formData = new FormData();
        formData.append("countyId", county_hidden.value);
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                var r = JSON.parse(xmlHttp.responseText);
                var html = "";
                for (var dist in r) {
                    if(dist != 'name') continue;
                    html = r[dist];
                }

                document.getElementById('input-search-countyCode').value = html;
            }
        }
        xmlHttp.open("post", "/getCountyById");
        xmlHttp.send(formData);
    }

</script>


