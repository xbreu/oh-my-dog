<?php

require_once "../core/ActiveRecord.php";

class Pet extends ActiveRecord {
    public $name;
    public $birth;
    public $gender;
    public $color;
    public $size;
    public $idPhoto;
    public $breedId;

    public function tableName() {
        return "Pet";
    }

    public function rules() {
        return [];
    }
}