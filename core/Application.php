<?php
require_once "../core/Controller.php";
require_once "../core/Request.php";
require_once "../core/Response.php";
require_once "../core/Router.php";

class Application {
    public static $ROOT_DIR;
    public static $BASE_DIR;
    public static $app;
    public $controller;
    public $request;
    public $response;
    public $router;

    public function __construct($rootPath) {
        self::$ROOT_DIR = $rootPath;
        self::$app = $this;
        $this->request = new Request();
        $this->response = new Response();
        $this->router = new Router($this->request, $this->response);
    }
    
    public static function isGuest(){
        return empty($_SESSION["customer"]);
    }
    
    public static function user(){
        return $_SESSION["customer"];
    }
    
    public static function goHome(){
        header('Location: /');
    }

    public function getController() {
        return $this->controller;
    }

    public function setController($controller) {
        $this->controller = $controller;
    }

    public function run() {
        echo $this->router->resolve();
    }
}
