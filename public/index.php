<?php
require_once "../core/Application.php";
require_once "../control/HomeController.php";
require_once "../control/SiteController.php";
require_once "../control/AuthController.php";
require_once "../control/DashboardController.php";
require_once "../control/PetController.php";
require_once "../control/ApiController.php";
require_once "../control/FavoriteController.php";

$folder = __DIR__ . "/../";
$app = new Application($folder);

session_start();

$app->router->get("", [HomeController::class, 'home']);
$app->router->post("", [HomeController::class, 'home']);

$app->router->get('login', [AuthController::class, 'login']);
$app->router->post('login', [AuthController::class, 'login']);
$app->router->get('register', [AuthController::class, 'register']);
$app->router->post('register', [AuthController::class, 'register']);
$app->router->post("logout", [AuthController::class, 'logout']);
$app->router->get("new-publication", [AuthController::class, 'createPublication']);
$app->router->post("new-publication", [AuthController::class, 'createPublication']);
$app->router->get("edit-publication", [AuthController::class, 'editPublication']);
$app->router->post("edit-publication", [AuthController::class, 'editPublication']);

$app->router->get("dashboard", [DashboardController::class, 'dashboard']);
$app->router->post("dashboard", [DashboardController::class, 'dashboard']);

$app->router->get("pets", [PetController::class, 'feed']);
$app->router->get("user-publications", [PetController::class, 'userPublications']);
$app->router->post("user-publications", [PetController::class, 'userPublications']);

$app->router->get("pet", [PetController::class, 'item']);
$app->router->post("pet", [PetController::class, 'item']);
$app->router->get("favorite", [FavoriteController::class, 'favorite']);

$app->router->get("myfavorites", [DashboardController::class, 'myfavorites']);
$app->router->get("myaccount", [DashboardController::class, 'myaccount']);
$app->router->post("myaccount", [DashboardController::class, 'myaccount']);
$app->router->get("mypublications", [DashboardController::class, 'mypublications']);

// API
$app->router->post("getCountiesByDistrictId", [ApiController::class, 'getCountiesByDistrictId']);
$app->router->post("getDistrictByCountyId", [ApiController::class, 'getDistrictByCountyId']);
$app->router->post("getCountyById", [ApiController::class, 'getCountyById']);

$app->run();
