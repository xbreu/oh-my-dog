<?php


abstract class ActiveRecord extends Model {
    abstract public function tableName();
    
    public function findAll($param = PDO::FETCH_OBJ) {
        $pdo = new PDO('sqlite:../database/main.db');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $pdo->query("SELECT * FROM " . $this->tableName());

        return $statement->fetchAll($param);
    }

    public function findWhere($searchs, $condition_par = 'OR') {
        $condition = $condition_par == 'OR' ? 'OR' : 'AND';
        $bind = [];
        $sql = "";
        if (!is_array($searchs) || empty($searchs)) {
            return false;
        }
        if (count($searchs) == 1) {
            foreach ($searchs as $search => $value) {
                $sql .= $search . " = :" . $search . " ";
                $bind[":" . $search] = $value;
            }
        } else {
            foreach ($searchs as $search => $value) {
                $sql .= $search . " = :" . $search . " " . $condition . " ";
                $bind[":" . $search] = $value;
            }
        }

        $pdo = new PDO('sqlite:../database/main.db');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $pdo->query("SELECT * FROM " . $this->tableName() . " WHERE " . $sql);

        $statement->execute($bind);

        return $statement->fetchAll(PDO::FETCH_OBJ);
    }


    public function insert() {
        $pdo = new PDO('sqlite:../database/main.db');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $values = get_object_vars($this);
        unset($values['errors']);
        foreach ($values as $field => $v) {
            $ins[] = ':' . $field;
        }

        $ins = implode(',', $ins);
        $fields = implode(',', array_keys($values));

        $sql = "INSERT INTO " . $this->tableName() . " ($fields) VALUES ($ins)";
        $sth = $pdo->prepare($sql);
        foreach ($values as $f => $v) {
            $sth->bindValue(':' . $f, $v);
        }

        try {
            $result = $sth->execute();
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (strpos($message, "UNIQUE constraint")) {
                $row = explode("constraint failed: ", $message)[1];
                $row = explode(".", $row)[1];
                $this->addError($row, "This element must be unique");
            } else if (strpos($message, "Integrity constraint")) {
                $row = explode("constraint failed: ", $message)[1];
                if (trim($row) == "validContact")
                    $this->addError("phone", "This element must be a valid phone number");
            } else {
                $this->addError("username", "Error " . $e->getCode());
            }
            return false;
        }

        return $result ? $pdo->lastInsertId() : false;
    }

    public function update() {
        $pdo = new PDO('sqlite:../database/main.db');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $values = get_object_vars($this);
        unset($values['errors']);
        $str = "";
        foreach ($values as $field => $v) {
            $str .= $field . ' = :' . $field . ", ";
        }
        $str = substr($str, 0, -2);

        $sql = "UPDATE " . $this->tableName() . " SET {$str} WHERE id = " . $values['id'];

        $sth = $pdo->prepare($sql);
        foreach ($values as $f => $v) {
            $data[$f] = $v;
        }

        $sth->execute($data);
    }

    public function remove() {
        $pdo = new PDO('sqlite:../database/main.db');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "DELETE FROM " . $this->tableName() . " WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        $stmt->execute();
    }
}