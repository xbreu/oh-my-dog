<?php

require_once "../core/Model.php";

class LoginForm extends Model {
    public $username;
    public $password;

    public function rules() {
        return [
            'username' => [[self::RULE_AUTH, 'auth' => 'password']]
        ];
    }
}
