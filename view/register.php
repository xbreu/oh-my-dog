<form action="" method="post">
    <i class="material-icons account-image">account_circle</i>
    <?php Form::write([
        ['type' => 'text', 'name' => 'username', 'label' => 'Username'],
        ['type' => 'text', 'name' => 'firstName', 'label' => 'First Name'],
        ['type' => 'text', 'name' => 'lastName', 'label' => 'Last Name'],
        ['type' => 'email', 'name' => 'email', 'label' => 'Email'],
        ['type' => "date", 'name' => 'birth', 'label' => 'Birthday'],
        ['type' => "text", 'name' => 'phone', 'label' => 'Phone Number'],
        [
            'type' => "select-search",
            'name' => 'districtCode',
            'label' => 'District',
            'options' => $districts,
            'callback' => 'appendCounty'
        ],
        ['type' => "select-search", 'name' => 'countyCode', 'label' => 'County'],
        ['type' => 'password', 'name' => 'password', 'label' => 'Password'],
        ['type' => 'password', 'name' => 'passwordConfirmation', 'label' => 'Password Confirmation'],
    ], $model); ?>
    
    <button type="submit">Register</button>
</form>

<script>
    var district = document.getElementById('input-search-districtCode');
    var district_hidden = document.getElementById('districtCode');

    var county = document.getElementById('input-search-countyCode');
    var county_hidden = document.getElementById('countyCode');
    county.disabled = true;
    function appendCounty(event) {
        county.disabled = true;
        var formData = new FormData();
        formData.append("districtId", district_hidden.value);
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                var r = JSON.parse( xmlHttp.responseText);
                var html = "";
                for (var county in r){
                    html += '<a onclick="setSelectSearch(\''+r[county]+'\', \''+county+'\', \'countyCode\')">'+r[county]+'</a>';
                }
                document.getElementById('select-search-countyCode').innerHTML = html;
            }
        }
        xmlHttp.open("post", "/getCountiesByDistrictId");
        xmlHttp.send(formData);
        county.disabled = false;
    }


</script>
