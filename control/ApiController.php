<?php
require_once "../core/Controller.php";
require_once "../model/Locality.php";

class ApiController extends Controller
{
    
    public function getCountiesByDistrictId(Request $request)
    {
        if ($request->isPost()) {
            $idDist = $request->getBody()['districtId'];
            $locality = new Locality('County');
            $counties = $locality->findWhere(['idDistrict' => $idDist]);
            $r = [];
            foreach ($counties as $county){
                $r[$county->id] = $county->name;
            }
            return json_encode($r);
        }
        return "";
    }
    
    public function getDistrictByCountyId(Request $request)
    {
        if ($request->isPost()) {
            $idCounty = $request->getBody()['countyId'];
            $county = new Locality('County');
            $county = $county->findWhere(['id' => $idCounty])[0];
            $locality = new Locality('District');
            $counties = $locality->findWhere(['id' => $county->idDistrict]);
            $r = [];
            foreach ($counties as $county){
                $r[$county->id] = $county->name;
            }
            return json_encode($r);
        }
        return "";
    }
    
    public function getCountyById(Request $request){
        if ($request->isPost()) {
            $idCounty = $request->getBody()['countyId'];
            $locality = new Locality('County');
            $counties = $locality->findWhere(['id' => $idCounty])[0];
            return json_encode($counties);
        }
        return "";
    }
}