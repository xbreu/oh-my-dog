<link rel="stylesheet" href="style/showPublication.css">

<div id='publication'>
    <!-- section left: images -->
    <section class=leftPart>
        <section class=pet_images>
            <?php
                $photo = new Photo();
                echo '<div class="pet-image" style="background-image: url(data:image/png;base64,'.base64_encode($photo->findWhere(['id' => $pet->idPhoto])[0]->img).')"></div>';
            ?>
            <ul>
                <li><img src="images/dog-teste.jpg" width="100" height="100" alt="pet photo"></li>
                <li><img src="images/dog-teste.jpg" width="100" height="100" alt="pet photo"></li>
                <li><img src="images/dog-teste.jpg" width="100" height="100" alt="pet photo"></li>
                <li><img src="images/dog-teste.jpg" width="100" height="100" alt="pet photo"></li>
            </ul>
        </section>
        <!-- Trigger/Open The Modal -->
        <button id="modal-button">Offer Proposal</button>

        <!-- The Modal -->
        <div id="proposal-modal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <span class="close">&times;</span>
                <form method="post">
                    <p>Write a message for <?= $user->firstName ?>: </p>
                    <textarea style="display: block" id="title" rows="2" cols="100" name="comment"
                              placeholder="write a comment......"></textarea>
                    <input type="hidden" name="publication" value="<?= $publication->id ?>">
                    <input type="hidden" name="proposal" value="true">
                    <input type="submit" value="Send">
                </form>
            </div>

        </div>
    </section>

    <!-- section right: information  -->
    <section class="info">
        <h1><i class="fas <?= ($pet->gender === "m") ? "fa-mars" : "fa-venus" ?>"></i> <b><?= $pet->name ?> </b>
            <?= !empty($pet->birth) ? ", " . (date('Y') - explode("-",
                        $pet->birth)[0]) . " years" : "" ?></h1>
        <p><?= $publication->description ?? "" ?></p>
        <p><b>Color: </b> <?= $pet->color ?></p>
        <p><b>Size: </b> <?= $pet->size ?></p>
        <p><b>Location: </b><?= $county->name ?></p>
        <p><b>Current Owner: </b> <?= $user->firstName ?></p>
    </section>
</div>

<form class="comment" method="post">
    <textarea id="title" rows="2" cols="4" name="comment"
              placeholder="write a comment......"></textarea>
    <input type="hidden" name="publication" value="<?= $publication->id ?>">
    <input type="submit" value="Comment"/>
</form>
<?php foreach ($comments as $comment) {
    if ($comment->proposal && $publication->idUser !== Application::user()['id'])
        continue;
    $commentUser = new Customer();
    $commentUser = $commentUser->findWhere(['id' => $comment->idUser])[0];
    echo "<p " . (($comment->proposal) ? "style=\"color:red\"" : "") . ">" . $commentUser->firstName . ":     ";
    echo $comment->comment . "</p>";
} ?>

<div id="display"></div>
<script type="text/javascript">
    let titles = [];
    let titleInput = document.getElementById("title");
    let messageBox = document.getElementById("display");

    function Allow() {
        if (!user.title.value.match(/[a-zA-Z]$/) && user.title.value != "") {
            user.title.value = "";
            alert("Please Enter only alphabets");
        }
        window.location.reload()
    }

    function insert() {
        titles.push(titleInput.value);
        clearAndShow();
    }

    function clearAndShow() {
        titleInput.value = "";
        messageBox.innerHTML = "";
        messageBox.innerHTML += " " + titles.join("<br/> ") + "<br/>";
    }
</script>