<?php

require_once "../core/Model.php";
require_once "Customer.php";

class SignUpForm extends Model {
    public $username;
    public $firstName;
    public $lastName;
    public $birth;
    public $phone;
    public $email;
    public $created;
    public $countyCode;
    public $password;
    public $passwordConfirmation;

    public function rules() {
        return [
            'username' => [self::RULE_REQUIRED, [self::RULE_SIZE, 'min' => 5, 'max' => 20]],
            'password' => [self::RULE_REQUIRED, [self::RULE_SIZE, 'min' => 5]],
            'passwordConfirmation' => [self::RULE_REQUIRED, [self::RULE_MATCH, 'match' => 'password']],
            'firstName' => [self::RULE_REQUIRED],
            'lastName' => [self::RULE_REQUIRED],
            'email' => [self::RULE_REQUIRED],
        ];
    }

    public function register() {
        $customer = new Customer();
        $customer->loadData($this);
        if ($customer->validate()) {
            $customer->password = password_hash($customer->password, PASSWORD_BCRYPT);
            
            if ($id_customer = $customer->insert() !== false) {
                return $id_customer;
            }
        }

        $this->errors = array_merge($this->errors, $customer->errors);

        return false;
    }
}
