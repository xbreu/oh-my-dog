<?php


class Comment extends ActiveRecord {
    public $idUser;
    public $idPublication;
    public $comment;
    public $proposal;
    
    public function rules() {
        return [
            'comment' => [self::RULE_REQUIRED]
        ];
    }

    public function tableName() {
        return "Comment";
    }
}