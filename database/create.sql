PRAGMA foreign_keys = ON;

drop table if exists Country;
create table Country
(
    code CHAR(2) PRIMARY KEY,
    name VARCHAR(64) UNIQUE NOT NULL
);

drop table if exists District;
create table District
(
    id          INTEGER PRIMARY KEY,
    name        VARCHAR(128) NOT NULL,
    nameCountry CHAR(2) REFERENCES Country ON DELETE RESTRICT ON UPDATE CASCADE
);

drop table if exists County;
create table County
(
    id         INTEGER PRIMARY KEY,
    name       VARCHAR(128) NOT NULL,
    idDistrict INTEGER REFERENCES District ON DELETE RESTRICT ON UPDATE CASCADE
);

drop table if exists Locality;
create table Locality
(
    localityCode INTEGER PRIMARY KEY,
    idCounty     INTEGER REFERENCES County ON DELETE RESTRICT ON UPDATE CASCADE
);

drop table if exists User;
create table User
(
    id         INTEGER PRIMARY KEY AUTOINCREMENT,
    username   VARCHAR(5, 20)     NOT NULL UNIQUE,
    password                      NOT NULL,
    firstName  VARCHAR(64)        NOT NULL,
    lastName   VARCHAR(64)        NOT NULL,
    birth      DATE               NOT NULL,
    phone      INTEGER UNIQUE     NOT NULL,
    email      VARCHAR(64) UNIQUE NOT NULL,
    created    DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    countyCode INTEGER REFERENCES County ON DELETE RESTRICT ON UPDATE CASCADE,
    photo      CHAR(32),
    CONSTRAINT validContact CHECK (phone >= 10000000)
);

drop table if exists Breed;
create table Breed
(
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    name    VARCHAR(64) NOT NULL,
    species VARCHAR(64) NOT NULL,
    family  VARCHAR(64) NOT NULL,
    class   VARCHAR(64) NOT NULL
);

drop table if exists Pet;
create table Pet
(
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    name    VARCHAR(64) NOT NULL,
    birth   DATE        NOT NULL,
    gender  CHAR(1),
    color   VARCHAR(20),
    size    VARCHAR(10),
    idPhoto INTEGER REFERENCES Photo ON DELETE RESTRICT ON UPDATE CASCADE,
    breedId INTEGER REFERENCES Breed ON DELETE RESTRICT ON UPDATE CASCADE
);

drop table if exists Publication;
create table Publication
(
    id           INTEGER PRIMARY KEY AUTOINCREMENT,
    description  VARCHAR(256),
    created      DATE NOT NULL DEFAULT (DATETIME('now')),
    idUser       INTEGER REFERENCES User ON DELETE SET NULL ON UPDATE CASCADE,
    idPet        INTEGER REFERENCES Pet ON DELETE RESTRICT ON UPDATE CASCADE,
    localityCode INTEGER REFERENCES Locality ON DELETE RESTRICT ON UPDATE CASCADE
);

drop table if exists Comment;
create table Comment
(
    idParent      INTEGER REFERENCES Comment ON DELETE SET NULL ON UPDATE CASCADE DEFAULT NULL,
    idUser        INTEGER REFERENCES User ON DELETE SET NULL ON UPDATE CASCADE,
    idPublication INTEGER REFERENCES Publication ON DELETE CASCADE ON UPDATE CASCADE,
    comment       VARCHAR(256),
    created       DATE NOT NULL                                                   DEFAULT (DATETIME('now')),
    proposal      BOOLEAN                                                         DEFAULT FALSE,
    PRIMARY KEY (idUser, idPublication, created)
);

drop table if exists Adoption;
create table Adoption
(
    created       DATE NOT NULL DEFAULT (DATETIME('now')),
    idPublication INTEGER REFERENCES Publication ON DELETE CASCADE ON UPDATE CASCADE PRIMARY KEY,
    idUser        INTEGER REFERENCES User ON DELETE SET NULL ON UPDATE CASCADE
);

drop table if exists Photo;
create table Photo
(
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    img  BLOB
);