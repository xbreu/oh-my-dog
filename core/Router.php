<?php
require_once "../core/Application.php";
require_once "../core/Controller.php";
require_once "../core/Request.php";
require_once "../core/Response.php";
require_once "../control/ErrorController.php";

class Router {
    public $request;
    public $response;
    protected $routes = [];

    public function __construct($request, $response) {
        $this->request = $request;
        $this->response = $response;
    }

    public function get($path, $callback) {
        $this->routes['get'][$path] = $callback;
    }

    public function post($path, $callback) {
        $this->routes['post'][$path] = $callback;
    }

    protected function layoutContent($layout = null) {
        if ($layout === null)
            $layout = Application::$app->controller->layout;

        include_once Application::$ROOT_DIR . "view/layout/$layout.php";
        return ob_get_clean();
    }

    protected function viewContent($view, $params) {
        foreach ($params as $key => $value) {
            $$key = $value;
        }
        ob_start();
        include_once Application::$ROOT_DIR . "view/$view.php";
        return ob_get_clean();
    }

    public function renderView($view, $params = []) {
        $layoutContent = $this->layoutContent();
        $viewContent = $this->viewContent($view, $params);
        return str_replace('{{content}}', $viewContent, $layoutContent);
    }

    public function resolve() {
        $path = $this->request->getPath();
        $method = $this->request->getMethod();
        $callback = $this->routes[$method][$path] ?? false;
        if ($callback === false) {
            $this->response->setStatusCode(404);
            Application::$app->controller = new ErrorController();
            return $this->renderView("not-found");
        }
        if (is_string($callback))
            return $this->renderView($callback);
        if (is_array($callback)) {
            Application::$app->controller = new $callback[0]();
            $callback[0] = Application::$app->controller;
        }
        return call_user_func($callback, $this->request);
    }
}
