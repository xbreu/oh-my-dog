<form action="" method="post" enctype="multipart/form-data">
    <label for="img"> <i class="material-icons add_photo_alternate">add_photo_alternate</i></label>
    <b>Upload a photo</b>
    <input type="file" id="img" name="img" accept="image/*">
    
    <?php Form::write([
        ['type' => 'text', 'name' => 'name', 'label' => 'Pet name'],
        ['type' => 'text', 'name' => 'color', 'label' => 'Color'],
        ['type' => "date", 'name' => 'birth', 'label' => 'Birthday'],
        [
            'type' => "select",
            'name' => 'size',
            'label' => 'Select the Size',
            'options' => [
                'small' => 'Small',
                'medium' => 'Medium',
                'large' => 'Large'
            ]
        ],
        [
            'type' => "select",
            'name' => 'breed',
            'label' => 'Select the Breed',
            'options' => [
                'small' => 'Dog',
                'medium' => 'Cat',
                'bird' => 'Bird',
                'reptile' => 'Reptile',
                'fish' => 'Fish',
            ]
        ],
        [
            'type' => "select",
            'name' => 'gender',
            'label' => 'Select the Gender',
            'options' => [
                'm' => 'Male',
                'f' => 'Female',
                'o' => 'Other'
            ]
        ],
    ], $model); ?>

    <label for="description">Description</label>
    <textarea name="description" rows="4">Use this area to describes a little about the personality and likes of the pet for the possible future owners.
    </textarea>

    <button type="submit">Submit</button>
</form>

