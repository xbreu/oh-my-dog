<?php

require_once "../core/ActiveRecord.php";

class Photo extends ActiveRecord
{
    public $id;
    public $img;

    public function tableName() {
        return "Photo";
    }

    public function rules() {
        return [];
    }
}