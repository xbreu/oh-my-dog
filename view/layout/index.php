<!DOCTYPE html>
<html lang="en">
<head>
    <?php include "../view/components/head.php" ?>
    <link rel="stylesheet" href="style/index.css">
</head>
<body>

<?php include "../view/components/nav.php" ?>
<div class="container">
    {{content}}
</div>

<?php include "../view/components/footer.php" ?>
</body>
</html>
