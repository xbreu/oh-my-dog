<?php

class Form
{
    public $fields = [];
    
    public static function write($inputs, $model = null)
    {
        foreach ($inputs as $input) {
            $label = $input['label'] ?? "";
            $type = $input['type'] ?? "";
            $name = $input['name'] ?? "";
            $class = $input['class'] ?? "";
            $options = $input['options'] ?? [];
            $callback = $input['callback'] ?? "";
            
            switch ($type) {
                case 'select-search':
                    ?>
                    <label>
                        <?= $label ?>
                        <div class="select-search">
                            <input type="hidden" name="<?= $name ?>" id="<?= $name ?>" value="<?= $model->$name ?>">
                            <input autocomplete="off" onclick="toggleOptions('<?= $name ?>')" type="text" placeholder="<?=  $label ?>" class="input-search-select" id="input-search-<?= $name ?>" onkeyup="filterOptions('<?= $name ?>')">
                            <div id="select-search-<?= $name ?>" class="select-search-content">
                                <?php foreach ($options as $value => $option) { ?>
                                    <a onclick="setSelectSearch('<?= $option ?>', '<?= $value ?>', '<?= $name ?>', <?= $callback ?>)"><?= $option ?></a>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        if (isset($model->errors[$name])) {
                            foreach ($model->errors[$name] as $error) { ?>
                                <?= $error ?>
                            <?php }
                        } ?>
                    </label>
                    <?php
                    break;
                
                case 'select':
                    ?>
                    <label>
                        <?= $label ?>
                        <select class="<?= $class ?>"
                            <?php if (isset($model->$name)) { ?>
                                value="<?= $model->$name ?>"
                            <?php } ?>
                                name="<?= $name ?>">
                            <option disabled selected><?= $label ?></option>
                            <?php foreach ($options as $value => $option) { ?>
                                <option value="<?= $value ?>"><?= $option ?></option>
                            <?php } ?>
                        </select>
                        <?php
                        if (isset($model->errors[$name])) {
                            foreach ($model->errors[$name] as $error) { ?>
                                <?= $error ?>
                            <?php }
                        } ?>
                    </label>
                    <?php
                    break;
                default:
                    
                    ?>
                    <label>
                        <?= $label ?>
                        <input class="<?= $class ?>" type="<?= $type ?>"
                            <?php if (isset($model->$name) && ($type !== 'password')) { ?>
                                value="<?= $model->$name ?>"
                            <?php } ?>
                               placeholder="<?= $label ?>"
                               name="<?= $name ?>">
                        <?php
                        if (isset($model->errors[$name])) {
                            foreach ($model->errors[$name] as $error) { ?>
                                <?= $error ?>
                            <?php }
                        } ?>
                    </label>
                <?php }
        }
    }
}
