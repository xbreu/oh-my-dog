<?php


require_once "../core/Model.php";
require_once "../core/ActiveRecord.php";


class Customer extends ActiveRecord {
    public $id;
    public $username;
    public $password;
    public $firstName;
    public $lastName;
    public $birth;
    public $phone;
    public $email;
    public $countyCode;

    public function rules() {
        return [
            'username' => [self::RULE_REQUIRED, [self::RULE_SIZE, 'min' => 5, 'max' => 20]],
            'password' => [self::RULE_REQUIRED, [self::RULE_SIZE, 'min' => 5]],
            'firstName' => [self::RULE_REQUIRED],
            'lastName' => [self::RULE_REQUIRED],
            'birth' => [self::RULE_REQUIRED],
            'email' => [self::RULE_REQUIRED],
            'phone' => [self::RULE_REQUIRED],
        ];
    }

    public function tableName() {
        return "User";
    }
}