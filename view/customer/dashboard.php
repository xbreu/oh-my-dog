<?php


?>
<div class="main-title">
    <h2>Hello, <?= $customer->firstName; ?>.</h2>
</div>
<section id="dashboard-menu">
    <!-- Add each category -->
    <?php foreach ($menu as $url => $option) { ?>
    <article>
        <a href="<?= $url ?>"><?= $option ?></a>
    </article>
    <?php } ?>
</section>