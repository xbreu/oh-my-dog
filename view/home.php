<section class="slideshow">
    <!-- Add all images in carousel folder -->
    <?php foreach ($slides as $key => $image) { ?>
        <div class="slide" style="background-image: url('images/carousel/<?= $image ?>')"></div>
        <!--<img class="slide" src="" alt="carousel image">-->
    <?php } ?>

    <!-- Creates a dot for each carousel image -->
    <div class="dots">
        <?php for ($i = 0; $i < sizeof($slides); $i++) { ?>
            <span class="dot" onclick="goToSlide(<?= $i ?>)"></span>
        <?php } ?>
    </div>
</section>

<section id="categories">
    <!-- Add each category -->
    <?php foreach ($categories as $key => $image) { ?>
        <article>
            <img src="images/categories/<?= $image ?>" alt="<?= $image ?> category">
            <footer><?= $key ?></footer>
        </article>
    <?php } ?>
</section>

<!--
<section id="testimonials">
    <h2>Testimonials</h2>
</section>
-->

<script src="script/carousel.js"></script>