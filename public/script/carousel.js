let slideIndex = 0
let interval
goToSlide(slideIndex)

function hide(slides, dots) {
    slides[slideIndex].style.display = "none"
    dots[slideIndex].className = dots[slideIndex].className.replace(" active", "")
}

function show(slides, dots) {
    slides[slideIndex].style.display = "block"
    dots[slideIndex].className += " active"
}

function currentSlide(n) {
    const slides = document.getElementsByClassName("slide")
    const dots = document.getElementsByClassName("dot")

    hide(slides, dots)
    if (n >= slides.length)
        n = 0
    slideIndex = n

    show(slides, dots)
}

function nextSlide() {
    currentSlide(slideIndex + 1)
}

function goToSlide(n) {
    window.clearInterval(interval)
    currentSlide(n)
    interval = window.setInterval(nextSlide, 4 * 1000)
}